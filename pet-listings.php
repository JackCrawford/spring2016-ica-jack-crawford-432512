<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Pet Listings</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
 
	<h1>Pet Listings</h1>
	
	<?php
		
		$mysqli = new mysqli('localhost', 'jackcrawford', 'phare2017', 'petdb');
 
        if($mysqli->connect_errno) {
			   printf("Connection Failed: %s\n", $mysqli->connect_error);
			   exit;
        }
		
		$stmt = $mysqli->prepare("select select species, count(species) from pets");
		if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
		}
		 
		$stmt->execute();
		 
		$stmt->bind_result($species, $numspecies);
		
		while($row = $result->fetch_assoc()){
		
			echo "<ul>\n";
			while($stmt->fetch()){
				printf("\t<li>%s %s</li>\n",
					htmlspecialchars($species),
					htmlspecialchars($numspecies)
				);
			}
			echo "</ul>\n";
		}
		
		$stmt->close();
		
		$stmt2 = $mysqli->prepare("select species, name, weight, description, filename from pets");
		if(!$stmt2){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
		}
		
		$stmt2->execute();
 
		$result = $stmt2->get_result();
		
		$stmt->bind_result($species2, $name, $weight, $description, $file);
		
		?>
		
		<img src="<?php echo file_dir . '/' . $file; ?>" height="100" width="100"/>
		
		<?php
		echo "<ul>\n";
			while($stmt->fetch()){
				printf("\t<li>%s %s</li>\n",
					htmlspecialchars($species2),
					htmlspecialchars($name),
					htmlspecialchars($weight),
					htmlspecialchars($description)
				);
			}
			echo "</ul>\n";
		
		
		?>
 
</div></body>
</html>